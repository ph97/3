package com.isoft.carInfo;
/**
 * 
 * @author Petar Hristov
 *
 */
public class Audi extends Car
{
	/**
	 * calls the Car's class constructor with super 
	 * @param carModel 
	 * @param carCountry
	 * @param carSpeed
	 * @param carDoors
	 */
	public Audi(String carModel,String carCountry,int carSpeed,int carDoors)
	{
		super(carModel,carCountry,carSpeed,carDoors);
	}

	@Override
	public void setCarPrice() 
	{
		carPrice=2*10000;
	}
}
