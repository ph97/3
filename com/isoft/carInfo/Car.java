package com.isoft.carInfo;
/**
 * 
 * @author Petar Hristov
 *
 */
public abstract class Car 
{
	String carModel;
	String carCountry;
	int carSpeed;
	int carDoors;
	double carPrice;
	
	public Car(String carModel, String carCountry, int carSpeed, int carDoors) 
	{
		this.carModel = carModel;
		this.carCountry = carCountry;
		this.carSpeed = carSpeed;
		this.carDoors = carDoors;
	}
	
	public void printCarSpecs()
	{
		System.out.println("***********************"
						  +"\nCar model:"+carModel
						  +"\nCar country:"+carCountry
						  +"\nCar speed:"+carSpeed
						  +"\nCar Doors:"+carDoors
						  +"\nCar Price:"+carPrice
						  +"\n***********************");
	}
	
	//abstract method that has to be implemented by the children of this class 
	public abstract void setCarPrice();
}
