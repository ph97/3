package com.isoft.carInfo;

import java.util.Scanner;
/**
 * 
 * @author Petar Hristov
 *
 */
public class CarMenu {
	Scanner userInput = new Scanner(System.in);
	String choiceCar;
	Car[] cars;
	String regExPattern="[0-9]";
	
	public CarMenu(Car[] cars)
	{
		this.cars=cars;
	}

public void start()
	{
		do
		{
			System.out.println("----------------------");
			System.out.println("1.Audi");
			System.out.println("2.BMW");
			System.out.println("3.Wolkswagen");
			System.out.println("4.Skoda");
			System.out.println("Choice:");		
			choiceCar = userInput.nextLine();
		    if(!(choiceCar.matches(regExPattern)))
		    {
		    	System.out.println("Please enter a digit(1-4)");
		    	start();
		    }
			System.out.println("----------------------");
		}while(Integer.parseInt(choiceCar)<1 || Integer.parseInt(choiceCar)>4);
		printCarInfo(Integer.parseInt(choiceCar)-1);
	}
	
	/**
	 * prints the car information
	 * @param choice determines which car it has to be printed
	 */
	public void printCarInfo(int choice)
	{
		cars[choice].setCarPrice();
		cars[choice].printCarSpecs();
		start();
	}
}
