package com.isoft.carInfo;
/**
 * 
 * @author Petar Hristov
 *
 */
public class MainClass {

	public static void main(String[] args)
    {
		//creating car objects
		Car audi = new Audi("A4","Germany",200,5);
		Car bmw = new BMW("M3","Germany",205,4);
		Car wolkswagen = new Wolkswagen("asd","Germany",210,3);
		Car skoda = new Skoda("ovf","Germany",215,5);
		
		//grouping the objects in an array
		Car[] cars= {audi,bmw,wolkswagen,skoda};
		
		CarMenu menu=new CarMenu(cars);
		menu.start();
    }
	
}
